***************************************
Álgebra Linear para Computação Quântica
***************************************

Neste capítulo, um resumo de Álgebra Linear voltado para Computação Quântica é apresentado. A teoria é apresentada usando-se a *notação de Dirac*, ou, *notação de braket*, uma notação utilizada largamente em Mecânica Quântica e que será necessária para o restante do trabalho. Essa notação é conveniente para se fazer contas e faz com que as diversas operações possíveis se encaixem naturalmente. 

Na Álgebra Linear abstrata, pode-se considerar espaços vetoriais sobre diversos corpos (ou escalares), que são estruturas algébricas com propriedades semelhantes aos números reais e complexos. Os espaços vetoriais podem ter dimensão finita ou infinita. 

Na Computação Quântica o interesse é voltado a espaços vetoriais de dimensão finita sobre o corpo dos números complexos. Isso permite a identificação do espaço com as :math:`n`-uplas de números complexos, o que simplifica grandemente a teoria. Os resultados resumidos neste capítulo estão situados nesse contexto. 

A principal referência para esse capítulo é :cite:t:`book:qcqi_nc`. Outra referência muito útil é :cite:t:`book:al_elon`, que possui um capítulo voltado a espaços vetoriais complexos. Livros texto clássicos de Álgebra Linear, como :cite:t:`book:al_steinbruch` também são úteis. Embora tenham ênfase em espaços vetoriais reais, a maioria das definições, resultados e demonstrações se transporta integralmente para os espaços vetoriais complexos. 

Será considerado um pré-requisito a este texto um curso de Álgebra Linear ao nível de graduação abordando-se os seguintes itens: espaços vetoriais, base e dimensão, transformações lineares, autovalores e autovetores. Por ter um caráter de resumo, os resultados apresentados, via de regra, não são acompanhados de suas demonstrações, as quais podem ser encontradas nos livros mencionados no parágrafo anterior.

Espaço Vetorial, Base e Dimensão
================================

Espaço Vetorial
---------------

O conjunto das :math:`n`-uplas :math:`(z_0, \ldots, z_{n-1})` de números complexos com a soma e o produto por escalar definidos entrada a entrada é um *Espaço Vetorial Complexo* e é denotado por :math:`\mathbb{C}^n`. É conveniente representar esses elementos por vetores coluna. Tem-se então:

.. math::

    \begin{bmatrix}z_0 \\ z_{1} \\ \vdots \\ z_{n-1}\end{bmatrix} + \begin{bmatrix}w_0 \\ w_{1} \\ \vdots \\ w_{n-1}\end{bmatrix} & = \begin{bmatrix}z_0 + w_0\\ z_{1} + w_1 \\ \vdots \\ z_{n-1} + w_{n-1}\end{bmatrix}  \text{ e}\\
    z \cdot \begin{bmatrix}z_0 \\ z_{1} \\ \vdots \\ z_{n-1}\end{bmatrix} & = \begin{bmatrix}z \cdot z_0 \\ z\cdot z_{1} \\ \vdots \\ z \cdot z_{n-1}\end{bmatrix}.
 
Em Mecânica Quântica, os vetores de :math:`\mathbb{C}^n` costumam ser usados na *notação de Dirac*, ou *notação de braket*:

.. math::

    \ket{\psi} = (z_0 , z_{1} , \ldots , z_{n-1} ) =  \begin{bmatrix}z_0 \\ z_{1} \\ \vdots \\ z_{n-1}\end{bmatrix}.


Um vetor :math:`\ket{\psi}` é chamado `ket` (em contraponto com :math:`\bra{\psi}`, que será definido posteriormente, e será chamado *bra*). 
 
Os vetores se comportam de maneira semelhante aos números no que diz respeito à soma e subtração. Em particular, a soma comuta :math:`\ket{\phi} + \ket{\psi} = \ket{\psi} + \ket{\phi}`, há um vetor nulo, denotado por :math:`0` ou :math:`\ket{\varnothing} = (0, \ldots , 0)` de tal forma que :math:`\ket{\psi} + 0 = \ket{\psi}` e, ainda, vale :math:`-\ket{\psi} = (-z_0, \dots , -z_{n-1}) = -1 \cdot \ket{\psi}`. O produto por escalar também se comporta de maneira semelhante ao produto numérico, e valem as propriedades distributivas :math:`z(\ket{\phi} + \ket{\psi}) = z \ket{\phi} + z \ket{\psi}` e :math:`(z+w)\ket{\psi} = z\ket{\psi} + w \ket{\psi}`, por exemplo.

.. admonition:: Espaço de estados de 1 qubit
    :class: note

    O conjunto 
    
    .. math:: 
        \mathbb{C}^2 = \left\{ \ket{\psi} = \begin{bmatrix}a \\ b\end{bmatrix} : a,b \in \mathbb{C}\right\}

    é um espaço vetorial com soma e produto por escalar dados por

    .. math::
    
        \begin{bmatrix}a_1 \\ b_1 \end{bmatrix} + \begin{bmatrix}a_2 \\ b_2 \end{bmatrix} & = \begin{bmatrix} a_1 + a_2 \\ b_1 + b_2 \end{bmatrix} \\
         z \cdot \begin{bmatrix} a \\ b \end{bmatrix} & = \begin{bmatrix}za \\ zb\end{bmatrix}.
    
    Esse espaço vetorial será largamente utilizado nos capítulos seguintes e descreve o espaço de estados de 1 *qubit*, o análogo do bit clássico. 

Base e Dimensão
---------------

Uma base para o espaço vetorial :math:`\mathbb{C}^n` é um conjunto de vetores *linearmente independentes* (LI) e que *geram o espaço*.  Demonstra-se que todas as bases de um espaço vetorial têm o mesmo número de elementos, e define-se a *dimensão* do espaço vetorial pelo número de elementos de uma base. 

O espaço vetorial :math:`\mathbb{C}^n` tem dimensão :math:`n`, isto é, todas as suas bases têm :math:`n` vetores. Uma base muito útil é a chamada *base computacional*, ou *base canônica* [#]_:

.. math::
    
    \ket{0} = \begin{bmatrix}1 \\ 0 \\ \vdots \\ 0\end{bmatrix}
    \text{, } \ket{1} = \begin{bmatrix}0 \\ 1 \\ \vdots \\ 0\end{bmatrix}\text{, }
    \dots\text{, } \ket{n-1} = \begin{bmatrix}0 \\ 0 \\ \vdots \\ 1\end{bmatrix}.


Na base computacional, um vetor :math:`\ket{\psi} = (z_0 , z_{1} , \dots , z_{n-1} )` é escrito como

.. math::

    \ket{\psi} = z_0 \ket{0} + z_1 \ket{1} + \dots + z_{n-1} \ket{n-1}


Numa base qualquer :math:`\beta = \big\{\ket{b_0}, \dots, \ket{b_{n-1}} \big\}`, qualquer vetor :math:`\ket\psi` pode ser escrito como combinação linear dos vetores dessa base. Os coeficientes da combinação linear, colocados em um vetor coluna, representam o vetor :math:`\ket\psi` escrito na base :math:`\beta`, conforme o esquema abaixo:

.. math::

    \ket{\psi} = a_0 \ket{b_0} + \dots + a_{n-1} \ket{b_{n-1}}  \Leftrightarrow \big[ \ket{\psi} \big]_\beta = \begin{bmatrix} a_0 \\ \vdots \\ a_{n-1}\end{bmatrix}_\beta .

O subscrito :math:`\beta` pode ser omitido se não houver risco de confusão. Normalmente omite-se esse subscrito quando se trata da base computacional.

.. admonition:: Bases para 1 qubit
    :class: note

    .. _base_qubit:

    Há especial interesse no espaço :math:`\mathbb{C}^2`. Este espaço modela um *qubit* --- o análogo quântico do bit --- a ser discutido em mais detalhes posteriormente. O espaço :math:`\mathbb{C}^2` tem dimensão 2 e admite, entre outras, as seguintes bases:

    .. math::

        \mathcal{I} = \mathcal{Z} &= \big\{ \ket{0}, \ket{1} \big\} \\
        \mathcal{X} &= \left\{ \ket{+} = \tfrac{1}{\sqrt{2}}( \ket{0} + \ket{1}),  \ket{-} = \tfrac{1}{\sqrt{2}}( \ket{0} - \ket{1})  \right\} \\
        \mathcal{Y} &= \left\{ \ket{+i} = \tfrac{1}{\sqrt{2}}( \ket{0} + i\ket{1}) , \ket{-i} = \tfrac{1}{\sqrt{2}}( \ket{0} -i \ket{1}) \right\}.

    Essa notação para as bases será justificada *a posteriori*.


.. admonition:: :math:`\mathcal{X}` é base para 1 qubit
    :class: note

    Para mostrar que
    
    .. math::
    
        \mathcal{X} = \left\{ \ket{+} = \tfrac{1}{\sqrt{2}}( \ket{0} + \ket{1}),  \ket{-} = \tfrac{1}{\sqrt{2}}( \ket{0} - \ket{1})  \right\}
    
    é base do espaço de estados de 1 qubit, deve-se mostrar que os vetores são LI (Linearmente Independentes) e geram o espaço :math:`\mathbb{C}^2`.

    * :math:`\mathcal{X}` **é LI**: Considere a combinação linear nula:

      .. math::
  
          a_0 \ket{+} + a_1 \ket{-} & = 0 \\
          a_0 \frac{1}{\sqrt{2}}( \ket{0} + \ket{1}) + a_1 \frac{1}{\sqrt{2}}( \ket{0} - \ket{1}) & = 0 \\
          \frac{a_0 + a_1}{\sqrt{2}} \ket{0} + \frac{a_0 - a_1}{\sqrt{2}} \ket{1} & = 0
  
      Tem-se que
  
      .. math::
  
          \begin{cases}
              \frac{a_0 + a_1}{\sqrt{2}} = 0 \\
              \frac{a_0 - a_1}{\sqrt{2}} = 0
          \end{cases}
          \implies
          \begin{cases}
              a_0 + a_1 = 0 \\
              a_0 - a_1 = 0
          \end{cases}
          \implies
          \begin{cases}
              a_0 = 0 \\
              a_1 = 0  
          \end{cases}.
  
      Portanto os coeficientes da combinação linear nula devem ser todos nulos, e isso significa que os vetores :math:`\ket{+}` e :math:`\ket{-}` são LI.

    * :math:`\mathcal{X}` **gera o espaço**: Seja :math:`\ket{\psi} = z_0 \ket{0} + z_1 \ket{1}` um vetor qualquer de :math:`\mathbb{C}^2`. Tenta-se escrever :math:`\ket{\psi}` como combinação linear de :math:`\ket{+}` e :math:`\ket{-}`. Se for possível, esses vetores geram o espaço.

      .. math::
          
          z_0 \ket{0} + z_1 \ket{1} & = a_0 \ket{+} + a_1 \ket{-} \\
          & = a_0 \frac{1}{\sqrt{2}}( \ket{0} + \ket{1}) + a_1 \frac{1}{\sqrt{2}}( \ket{0} - \ket{1}) \\
          & = \frac{a_0 + a_1}{\sqrt{2}} \ket{0} + \frac{a_0 - a_1}{\sqrt{2}} \ket{1}

      Assim,

      .. math::

          \begin{cases}
            \frac{a_0 + a_1}{\sqrt{2}} = z_0 \\
            \frac{a_0 - a_1}{\sqrt{2}} = z_1
          \end{cases}
          \implies
          \begin{cases}
            a_0 = \frac{z_0 + z_1}{\sqrt{2}} \\
            a_1 = \frac{z_0 - z_1}{\sqrt{2}}
          \end{cases}.

      Dessa forma, :math:`\mathcal{X}` gera o espaço :math:`\mathbb{C}^2` e é base desse espaço.

    * **Comentário**: Quando se sabe previamente que a dimensão do espaço é :math:`n` e se a lista de vetores candidatos a base tem :math:`n` elementos, então as condições de ser LI e gerar o espaço são equivalentes. Em consequência, basta verificar uma das condições para mostrar que os vetores formam uma base. Por exemplo, se sabemos que a dimensão de :math:`\mathbb{C}^2` é :math:`\dim \mathbb{C}^2 = 2`, e temos que :math:`\mathcal{X}` tem dois elementos, então bastaria verificar uma das duas condições: :math:`\mathcal{X}` é LI ou :math:`\mathcal{X}` gera o espaço.

Matriz de Mudança de Base
-------------------------

Por vezes é conveniente expressar um vetor em outra base que não a base computacional. Essa mudança de base pode trazer novo *insight* em algumas situações, bem como tornar os cálculos mais simples e factíveis.

A matriz de mudança de base de :math:`\beta_{\text{old}} = \big\{\ket{u_0}, \ldots, \ket{u_{n-1}} \big\}` para :math:`\beta_{\text{new}} = \big\{ \ket{v_0}, \ldots, \ket{v_{n-1}} \big\}` é dada por

.. math::

    [I]^{\beta_{\text{old}}}_{\beta_{\text{new}} } = \left[ \begin{matrix} | & & | \\ \big[\ket{v_0}\big]_{\beta_{\text{old}}} & \cdots & \big[\ket{v_{n-1}}\big]_{\beta_{\text{old}}} \\  | & & | \end{matrix} \right].

Isto é, para montar a matriz de mudança de base, os vetores da base nova devem ser escritos como combinação linear dos vetores da base antiga, obtendo vetores coluna. Esses vetores serão as colunas da matriz de mudança de base. Dessa forma, tem-se

.. math::

   [v]_{\beta_{\text{new}}} = [I]^{\beta_{\text{old}}}_{\beta_{\text{new}} } [v]_{\beta_{\text{old}}}.

A matriz de mudança de base admite matriz inversa, que corresponde à mudança de base da base nova de volta para a base antiga:

.. math::

    {[I]^{\beta_{\text{old}}}_{\beta_{\text{new}}}}^{-1} = [I]^{\beta_{\text{new}}}_{\beta_{\text{old}} }.

.. admonition:: Matriz de Hadamard
    :class: note

    Considere as bases :math:`\mathcal{I}` e :math:`\mathcal{X}` apresentadas no exemplo "Bases para 1 qubit". A matriz de mudança de base da base computacional :math:`\mathcal{I}` para a base :math:`\mathcal{X}` é obtida escrevendo os vetores da base nova (:math:`\mathcal{X}`) como combinação linear dos vetores na base antiga (:math:`\mathcal{I}`):

    .. math::

        \ket{+}
        & = \frac{1}{\sqrt{2}} \ket{0} + \frac{1}{\sqrt{2}}  \ket{1}  = \begin{bmatrix}\frac{1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}}\end{bmatrix}_{\mathcal{I}}\\
        \ket{-}
        & = \frac{1}{\sqrt{2}} \ket{0} - \frac{1}{\sqrt{2}} \ket{1} = \begin{bmatrix}\phantom{-} \frac{1}{\sqrt{2}} \\  - \frac{1}{\sqrt{2}}\end{bmatrix}_{\mathcal{I}}

    Colocam-se as os vetores coluna na ordem em que aparecem na lista:

    .. math::

        [I]^\mathcal{I}_\mathcal{X} = \begin{bmatrix}\begin{bmatrix} \frac{1}{\sqrt{2}} \\ \frac{1}{\sqrt{2}}\end{bmatrix}_{\mathcal{I}} & \begin{bmatrix} \phantom{-} \frac{1}{\sqrt{2}} \\  - \frac{1}{\sqrt{2}} \end{bmatrix}_{\mathcal{I}} \end{bmatrix} = \frac{1}{\sqrt{2}} \begin{bmatrix}1 & \phantom{-}1 \\ 1 & -1\end{bmatrix}.

    Essa matriz é conhecida em Computação Quântica como *matriz de Hadamard*, e costuma ser denotada por :math:`H`. Portanto

    .. math::

        H = [I]^\mathcal{I}_\mathcal{X} = \frac{1}{\sqrt{2}} \begin{bmatrix}1 & \phantom{-}1 \\ 1 & -1\end{bmatrix}.

    Também vale que a matriz de mudança de base de :math:`\mathcal{X}` para:math:`\mathcal{I}` é :math:`H`, visto que

    .. math::

        HH = I  \implies H^{-1} = H \implies [I]_\mathcal{I}^\mathcal{X} = \big([I]^\mathcal{I}_\mathcal{X} \big)^{-1} = H^{-1} = H.

    Então:

    .. math::

        \begin{array}{l}
            H\ket{0} = \ket{+} \\
            H\ket{1} = \ket{-}
        \end{array}
        \quad \quad
        \begin{array}{l}
            H\ket{+} = \ket{0} \\
            H\ket{-} = \ket{1}
        \end{array}.


.. rubric:: Notas de rodapé

.. [#] O adjetivo "canônico", na Matemática, tem um sentido de "padrão", como na expressão "configuração padrão".
