##################################################################################
Computação Quântica: Uma abordagem para estudantes de graduação em Ciências Exatas
##################################################################################

.. rubric:: Prefácio

Desde a década de 1980 até os dias atuais, a Computação Quântica tem se mostrado relevante área de pesquisa, com crescimento acelerado na atualidade. Universidades, empresas e startups têm projetos de pesquisa nessa área, e há expectativas de que a Computação Quântica seja comercialmente viável a curto/médio prazo. Espera-se que um computador quântico tenha desempenho superior ao computador tradicional (chamado de "clássico") para algumas importantes classes de problemas, além de menor consumo energético, por se tratar de uma computação *reversível*. 

O presente trabalho traz uma introdução à Computação Quântica. O objetivo é trazer um material acessível a estudantes de graduação em Ciências Exatas. Os capítulos 1 e 2 trazem pré-requisitos de Álgebra Linear e Mecânica Quântica. Uma introdução à Computação Quântica é apresentada no capítulo 3, e alguns dos principais algoritmos são apresentados no capítulo 4, como os algoritmos de Deutsch-Jozsa, de Simon, e de Grover.

.. rubric:: Sumário

.. toctree::
   :maxdepth: 3

   intro
   al
   ref